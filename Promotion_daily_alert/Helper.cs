﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Promotion_daily_alert
{
    public static class Helper
    {
        public static List<AlertMessage> GetRemarkableAlertMessages(DateTime dt)
        {
            List<AlertMessage> result = null;

            using (AutoNotesEntities context = new AutoNotesEntities())
            {
                var nsvs = context.Promotions.Where(x => x.DateCreated > dt || x.DateCreated > dt).Select(x => x).ToList();
                if (nsvs != null)
                {
                    result = new List<AlertMessage>();
                    foreach (var n in nsvs)
                    {
                        AlertMessage a = new AlertMessage();
                        a.Firstname = n.Firstname == null ? "" : n.Firstname;
                        a.Lastname = n.Lastname == null ? "" : n.Lastname;
                        a.Email = n.Email == null ? "" : n.Email;
                        a.Organization = n.Organization == null ? "" : n.Organization;
                        a.Twitter = n.Twitter == null ? "" : n.Twitter;
                        a.Role = n.Role == null ? "" : n.Role;

                        result.Add(a);
                    }
                }

            }

            return result;
        }
    }
}
